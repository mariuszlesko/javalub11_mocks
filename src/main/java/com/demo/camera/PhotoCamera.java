package com.demo.camera;

import static java.lang.Thread.sleep;

public class PhotoCamera implements WriteListener{
    boolean camerIsOn = false;
    ImageSensor sensor;
    Card card;
    boolean writeInProgress = false;
    boolean sensorShouldBeShutDown = false;

    public PhotoCamera(ImageSensor sensor, Card card) {
        this.sensor = sensor;
        this.card = card;
    }

    public void turnOn() {
        this.camerIsOn = true;
        sensor.turnOn();
    }

    public void turnOff() {
        if (!writeInProgress) {
            this.camerIsOn = false;
            sensor.turnOff();
        } else {
            this.sensorShouldBeShutDown = true;
        }
    }

    public void pressButton() {
        if (camerIsOn) {
            writeInProgress = true;
            byte[] dataFromSensor = sensor.read();

            card.write(dataFromSensor);
        }
    }

    @Override
    public void writeCompleted() {
        writeInProgress = false;
        if(this.sensorShouldBeShutDown){
            sensor.turnOff();
            this.camerIsOn = false;
        }
    }
}

