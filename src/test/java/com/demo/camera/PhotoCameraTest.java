package com.demo.camera;

import javafx.scene.Camera;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class PhotoCameraTest {

    @Test //1
    public void powerOnCameraShouldEnableSensorPower_sendSignalPowerOnToSensor(){
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);


        PhotoCamera camera = new PhotoCamera(sensor, card);
        camera.turnOn();

        Mockito.verify(sensor).turnOn();
    }

    @Test //2
    public void whenCameraIsPoweredOffSensorPowerIsTooPoweredOff(){
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);

        PhotoCamera camera = new PhotoCamera(sensor,card);
        camera.turnOff();

        Mockito.verify(sensor).turnOff();
    }

    @Test //3
    public void whenPowerIsOffPressedButtonIsWithoutAction(){

        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);

        PhotoCamera camera = new PhotoCamera(sensor, card);
        camera.turnOff();
        camera.pressButton();

        Mockito.verify(sensor, times(0)).read();
    }

    @Test //4
    public void whenCameraIsPowerOnAndButtonIsPressedDataFromSensorIsWritedToCard() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);

        PhotoCamera camera = new PhotoCamera(sensor, card);
        Mockito.when(sensor.read()).thenReturn(new byte[]{1, 2, 3, 4});
        camera.turnOn();
        camera.pressButton();

        Mockito.verify(sensor).read();
        Mockito.verify(card).write(Mockito.any());
    }

    @Test //5
    public void whenDataIsWriteToCardAfterButtonPressingCameraIsNotShutDown() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);


        PhotoCamera camera = new PhotoCamera(sensor, card);
        Mockito.when(sensor.read()).thenReturn(new byte[]{1, 2, 3, 4});

        camera.turnOn();
        camera.pressButton();
        camera.turnOff();


        Mockito.verify(sensor, times(0)).turnOff();
    }

    @Test //6
    public void whenWriteDataToCardIsDonePowerIsOff() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);


        PhotoCamera camera = new PhotoCamera(sensor, card);
        Mockito.when(sensor.read()).thenReturn(new byte[]{1, 2, 3, 4});

        camera.turnOn();
        camera.pressButton();

        camera.turnOff();
        camera.writeCompleted();


        Mockito.verify(sensor).turnOff();
        //photo czmerz is still on
    }

}